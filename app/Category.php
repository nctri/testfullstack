<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'face_category';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'name',
    ];

    protected $guarded = [];
}
