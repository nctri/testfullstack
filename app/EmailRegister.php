<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailRegister extends Model
{
    protected $table = 'email_register';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'email',
    ];

    protected $guarded = [];
}
