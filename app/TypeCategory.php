<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCategory extends Model
{
    protected $table = 'type_category';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'name',
    ];

    protected $guarded = [];
}

