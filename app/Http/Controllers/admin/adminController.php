<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use Validator;

class adminController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function settingStore()
    {
        $setting = Setting::get()->toArray();
        $str = $setting[0]['business_social'];
        $social = explode(";", $str);
        return view('admin.setting.setting',compact('setting','social'));
    }
    public function postSettingStore(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
        ]);

        if ($validator->passes()) {
            toastr()->info('Em chưa hoàn thành chức năng vừa rồi!', 'SORRY!');
            return redirect()->route('adminIndex');
        }
        else{
            toastr()->error('Please check info again!', 'ERROR!');
            return redirect()->back();
        }
    }
}
