<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use DB;

class loginController extends Controller
{
    public function getLogin()
    {
        return view("admin.login");
    }

    public function postLogin(Request $request)
    {
        $uname = $request->email;
        $password = $request->password;
        if (Auth::attempt(array('email' => $uname, 'password' => $password))){
            return redirect()->route('adminIndex');
        }
        else {        
            toastr()->error('Email or Password is wrong!');
            return redirect()->route('getLogin');
        }
        
    }
    

    public function postLogout()
    {
        if (Auth::logout()) {
            return redirect()->route('getLogin');
        }
        return redirect()->route('getLogin');
    }
}
