<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Menu;
use App\Category;
use App\Product;
use App\TypeCategory;
use App\EmailRegister;
use Validator;
use DB;


class homeCotroller extends Controller
{
    public function index()
    {
        $theloai = TypeCategory::get()->toArray();
        $mat = Category::get()->toArray();
        $product = Product::where('status',1)->get()->toArray();
        return view('client.index',compact('theloai','mat', 'product'));
    }

    public function postSelectFace(Request $request)
    {
        // dd($request->all());
        $idMat = $request->idMat;
        $idLoai = $request->idLoai;

        if($idMat==0 && $idLoai==0)
        {
            $product = Product::where('status',1)->get()->toArray();
        }
        if($idMat!=0 && $idLoai==0)
        {
            $product = Product::where('status',1)->where('face_cate_id',$idMat)->get()->toArray();
        }
        if($idMat==0 && $idLoai!=0)
        {
            $product = Product::where('status',1)->where('type_cate_id',$idLoai)->get()->toArray();
        }
        if($idMat!=0 && $idLoai!=0)
        {
            $product = Product::where('status',1)->where('type_cate_id',$idLoai)->Where('face_cate_id',$idMat)->get()->toArray();
        }
        
        
        return response()
            ->json(['status' => 1, 'data' => $product ]);
        
    }

    public function postCusRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->passes()) {
            $email = $request->email;
            $checkExit=DB::table('email_register')->where('email',$email)->count();

            if($checkExit == 0){
                $checkInsert=DB::table('email_register')->insert(['email' => $email]);

                if($checkInsert){
                    return response()
                        ->json(['status' => 1, 'data' => "Bạn đã đăng kí thành công" ]);
                }
                else{
                    return response()
                        ->json(['status' => 0, 'data' => "Có lỗi xảy ra!" ]);
                }
            }
            else{
                return response()
                    ->json(['status' => 1, 'data' => "Email đã được đăng kí trước đó" ]);
            }
            
        }
        else{
            return response()
                ->json(['status' => 0, 'data' => "Vui lòng kiểm tra thông tin email" ]);
        }
        
    }
}
