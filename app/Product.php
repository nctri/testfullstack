<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'cate_id',
        'product_image',
        'name',
        'short_description',
        'long_description',
        'home_type',
    ];

    protected $guarded = [];
}
