<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $table = 'setting';

    public $timestamps = true;

    protected $fillable = [
        'id',
        'logo',
        'business_name',
        'business_phone',
        'business_email',
        'business_social',
        'business_address',
    ];

    protected $guarded = [];
}
