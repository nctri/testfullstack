@extends('client.master')
@section('content')
{{-- {{dd($info)}} --}}
  <!-- du an tieu bieu -->
  <div class="duan">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="title-navigation">
            <p>
              <span class="font-muli">Trang chủ </span
              ><span class="color-index font14 font-muli"
                ><i>/ Các dự án tre nghệ</i></span
              >
            </p>
          </div>
        </div>
      </div>

      <div class="row row-header">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <div class="deg-block-left-trenghe">
            <div class="header-trenghe">
              <h4 class="color-index font-monter font-bold">
                Các dự án tiêu biểu của Tre Nghệ
              </h4>
            </div>
            <div class="des-trenghe">
              <p class="font-muli">
                Tre Nghệ hoạt động từ 6/2003 với sứ mệnh “WE DO FOR BETTER
                LIFE” – “Chúng ta làm cuộc sống tốt đẹp hơn”. Luôn lắng nghe,
                luôn thấu hiểu mong muốn khách hàng để kiến tạo những công
                trình thiết kế đẹp cho khách hàng cũng như sự hài lòng trong
                quá trình sử dụng sau này.
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <div class="deg-block-right-trenghe">
            <div class="header-img-trenghe">
              <img src="./images/duantieubieu.png" alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end du an -->

  <!-- show home -->
  <div class="show-home">
    <div class="container">
      <!-- filter -->
      <div class="block-filter">
        <div class="action">
          <div class="text-left">
            <span class="font-bold">Filter lọc dự án</span>
          </div>
          <div class="action-right">
            <div class="mattien">
              <div class="input-group mb-3">
                <select
                  id="select-face"
                  class="form-control"
                  style="background-image: url(./images/mattien.png);"
                >
                    <option value="0">Vị Trí</option>
                    @if($mat)
                        @foreach($mat as $keym => $valuem)
                            <option value="{{$valuem['id']}}">{{$valuem['name']}}</option>
                        @endforeach
                    @endif
                </select>
              </div>
            </div>

            <div class="theloainha ml-3">
              <div class="input-group mb-3">
                <select
                  id="select-type"
                  class="form-control"
                  style="background-image: url(./images/theloainha.png);"
                >
                  <option value="0">Thể loại nhà</option>
                    @if($theloai)
                        @foreach($theloai as $keytl => $valuetl)
                            <option  value="{{$valuetl['id']}}">{{$valuetl['name']}}</option>
                        @endforeach
                    @endif
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end filter -->

      <!-- all home -->
      @if($product)
        <div class="row row-home" data-img="{{asset('/')}}images/services/">
          @foreach($product as $keyPro => $valuePro)
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 select-cate face_{{$valuePro['face_cate_id']}} type_{{$valuePro['type_cate_id']}}">
            <div class="home-block">
              <div class="home-img w-100">
                <img src="{{asset('/')}}images/services/{{$valuePro['product_image']}}" alt="" class="w-100" />
              </div>
              <div class="home-detail">
                <div class="home-title m-0 p-0 font14 font-bold font-monter">
                  <span class="color-index upper">{{$valuePro['name']}}</span>
                </div>
                <div class="home-des font-monter font-w500">
                  {{$valuePro['short_description']}}
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      @endif
      <!-- end -->
    </div>
  </div>
  <!-- show home -->

  <!-- banner lien he -->
  <div class="banner-contact over-hide">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 pb-3">
          <div class="title-contact-now">
            <h4 class="font-monter font32 font-bold">
              Tre Nghệ đưa ngôi nhà trong mơ ra khỏi bản vẽ.
            </h4>
          </div>
        </div>
        <div
          class="col-xs-12 col-sm-12 col-md-12 col-lg-7 text-right pb-3 relative"
        >
          <a href="tel:@if($info && $info[0]['business_phone']) {{$info[0]['business_phone']}} @endif">
            <div class="btn-contact-now">
              <img src="./images/lienhengay.png" alt="" class="w-100" />
            </div>
          </a>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="img-contact-now">
            <img src="./images/banner-contact.png" alt="" />
            <img src="./images/khung.png" alt="" class="img-abs" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end -->

  <script>
    $(document).ready(function(){

      $('#select-face').on('change', function() {
        let type=1;
        let idMat = this.value;
        let idLoai =  $('#select-type').val();
        selectAction(type,idMat,idLoai);
      });

      $('#select-type').on('change', function() {
        let type2=2;
        let idLoai2 = this.value;
        let idMat2 =  $('#select-face').val();
        selectAction(type2,idMat2,idLoai2);
      });


      function selectAction(t,im,il){
        let dataImg = $('.row-home').attr('data-img');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{route("postSelectFace")}}',
            data: {
              type : t,
              idMat: im,
              idLoai: il,
            },
            dataType: 'json',
            success: function(data) {
              if(data.status==1){
                let fil =data.data;
                var htmlItem = fil.map(function(item){
                  return `<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                            <div class="home-block">
                              <div class="home-img w-100">
                                <img src="${dataImg}${item.product_image}" alt="" class="w-100" />
                              </div>
                              <div class="home-detail">
                                <div class="home-title m-0 p-0 font14 font-bold font-monter">
                                  <span class="color-index upper">${item.name}</span>
                                </div>
                                <div class="home-des font-monter font-w500">
                                  ${item.short_description}
                                </div>
                              </div>
                            </div>
                          </div>
                        `
                });

                var html =htmlItem.join('');
                $('.row-home').html(html);
                // toastr.success('succeed', data.status);

              }
            },
            error: function(data) {
              console.log(data);
            }
        })
      }


    });
  </script>
@endsection