<!DOCTYPE html>
<html lang="en">
  <head>
    <title>
        @if($info && $info[0]['business_name'])
            {{$info[0]['business_name']}}
        @endif
    </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    />
    <link
      href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,577;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,577;1,600;1,700;1,800;1,900&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
      integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
      crossorigin="anonymous"
    />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('/')}}css/style.min.css" />
    @toastr_css
  </head>
  <body>
      
    <!-- navbar -->
    <div class="deg-bg-nav">
      <div class="deg-nav w-100" id="deg-nav">
        <nav
          class="navbar navbar-expand-lg navbar-dark static-top"
          id="deg-sticky"
        >
          <div class="container deg-ctn-nav">
            <a class="navbar-brand p-0 test-logo" href="#">
              @if($info && $info[0]['logo'])
                <img src="{{asset('/')}}images/{{$info[0]['logo']}}" alt="" class="w-100 svg" />
              @endif
              
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarResponsive"
              aria-controls="navbarResponsive"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <div class="d1"></div>
              <div class="d2"></div>
              <div class="d3"></div>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">

                {{-- {{dd($menu)}} --}}
                @if($menu)
                    @foreach($menu as $key=>$value)
                        <li class="nav-item {{$key==0 ? 'active' : ''}}">
                            <a class="nav-link" href="{{url()->current()}}{{$value['url']}}">{{$value['name']}}</a>
                        </li>
                    @endforeach
                @endif
                <li class="nav-item">
                  <a class="nav-link font-roboto" href="#">
                    <img src="./images/search.png" class="test-search" alt="" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <!-- end navbar -->

    @yield('content')

    

    <!-- footer -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="footer-logo">
              <img src="./images/logo.png" alt="" class="w-100" />
            </div>
            <div class="des-for-logo">
              <span class="font16 font-w500 color-index font-san"
                >Công ty tư vấn và thiết kế kiến trúc xây dựng nhà đẹp, nội thất
                uy tín</span
              >
            </div>
            <div class="footer-contact-icon pb-4">
              @php
                $social = [];
                if($info && $info[0]['business_social'])
                {
                  $social = explode(";", $info[0]['business_social']);
                }
                $ss="intagram.png";
              @endphp
              
              @foreach ($social as $keySo => $valueSo)
                @php
                    if($keySo == 1){
                      $ss="zalo.png";
                    }
                    if($keySo == 2){
                      $ss="fb.png";
                    }
                @endphp
                <a href="{{$valueSo}}">
                  <div class="ft-bl">
                    <img src="./images/{{$ss}}" class="w-100" alt="" />
                  </div>
                </a>
              @endforeach
              

            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="footer-title">
              <span class="font-san font16 font-bold text-center color-text"
                >SITEMAP</span
              >
            </div>
            <div class="footer-menu">
              <ul>
                @if($menu)
                    @foreach($menu as $key=>$value)
                        <li>
                            <a href="{{url()->current()}}{{$value['url']}}" class="font16 font-san">{{$value['name']}}</a>
                        </li>
                    @endforeach
                @endif
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="footer-title">
              <span class="font-san font16 font-bold text-center color-text"
                >VỀ TRE NGHỆ</span
              >
            </div>
            <div class="footer-menu">
              <ul>
                {{-- {{dd($info)}} --}}
                @if($info[0]['business_address'])
                    <li>
                    <a class="font16 font-san"
                        ><img
                        src="./images/location.png"
                        alt=""
                        style="width: 23px;"
                        />
                        {{$info[0]['business_address']}}</a
                    >
                    </li>
                @endif
                @if($info && $info[0]['business_phone'])
                    <li>
                    <a class="font16 font-san"
                        ><img
                        src="./images/phone.png"
                        alt=""
                        style="width: 23px;"
                        />
                        {{$info[0]['business_phone']}}</a
                    >
                    </li>
                @endif
                @if($info && $info[0]['business_email'])
                    <li>
                    <a class="font16 font-san"
                        ><img
                        src="./images/email.png"
                        alt=""
                        style="width: 23px;"
                        />{{$info[0]['business_email']}}</a
                    >
                    </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="footer-title">
              <span class="font-san font16 font-bold text-center color-text"
                >ĐĂNG KÍ THEO DÕI</span
              >
            </div>
            <div class="footer-menu">
              <form class="register-email">
                <div class="form-group">
                  <input
                    type="email"
                    class="form-control"
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                    name="emailregister"
                  />
                </div>

                <button type="submit" class="btn btn-register btn-cus">Đăng kí</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row copyright">
      <div
        class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center font-san font16"
      >
        TreNghe &copy; 2020 by Eastplayers
      </div>
    </div>
    <!-- footer -->
  </body>
    @toastr_js
    @toastr_render
  <script>
    $(document).ready(function(){
      
      $(document).on('click','.btn-register',function(e){

        e.preventDefault();
        var email = $('input[name="emailregister"]').val();
        // console.log(email);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{route("postCusRegister")}}',
            data: {
              email : email,
            },
            dataType: 'json',
            success: function(data) {
              if(data.status==1){
                toastr.success(data.data, 'Succeed');
              }
              if(data.status==0){
                toastr.error(data.data, 'Error');
              }
            },
            error: function(data) {
              toastr.error("Có lỗi xãy ra! ", 'Error');
            }
        })
      })
        
    })
  </script>
</html>
