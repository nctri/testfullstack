@extends('admin.master') @section('content')

{{-- {{ dd($setting) }} --}}
<div class="container">
    <form action="{{route('postSettingStore')}}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Business Name <span style="color: red">*</span></label>
                <input
                    type="text"
                    class="form-control"
                    name="name"
                    placeholder="enter business name"
                    value="{{ $setting[0]['business_name'] ? $setting[0]['business_name'] : ''}}"
                />
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Business Phone <span style="color: red">*</span></label>
                <input
                    type="text"
                    class="form-control"
                    name="phone"
                    placeholder="Enter Phone"
                    value="{{ $setting[0]['business_phone'] ? $setting[0]['business_phone'] : ''}}"
                />
            </div>
        </div>

        <div class="form-group">
            <label for="inputAddress">Business Address <span style="color: red">*</span></label>
            <input
                type="text"
                class="form-control"
                name="address"
                placeholder="Enter Address"
                value="{{ $setting[0]['business_address'] ? $setting[0]['business_address'] : ''}}"
            />
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAddress">Intagram</label>
                <input
                    type="text"
                    class="form-control"
                    name="intagram"
                    placeholder="Enter Intagram"
                    value="{{ $social[0] != '' ? $social[0] : ''}}"
                />
            </div>
            <div class="form-group col-md-6">
                <label for="inputAddress">Facebook</label>
                <input
                    type="text"
                    class="form-control"
                    name="facebook"
                    placeholder="Enter Facebook"
                    value="{{ $social[1] != '' ? $social[1] : ''}}"
                />
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAddress">Zalo</label>
                <input
                    type="text"
                    class="form-control"
                    name="zalo"
                    placeholder="Enter Zalo"
                    value="{{ $social[2] != '' ? $social[2] : ''}}"
                />
            </div>
            <div class="form-group col-md-6">
                <label for="inputAddress">Business Email</label>
                <input
                    type="Email"
                    class="form-control"
                    name="email"
                    placeholder="Enter Email"
                    value="{{ $setting[0]['business_email'] ? $setting[0]['business_email'] : ''}}"
                />
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@endsection
