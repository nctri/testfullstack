<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            @if($info && $info[0]['business_name'])
            {{ $info[0]["business_name"] }}
            @endif
        </title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        />
        <link
            href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
            rel="stylesheet"
        />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,577;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,577;1,600;1,700;1,800;1,900&display=swap"
            rel="stylesheet"
        />
        <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
            integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
            crossorigin="anonymous"
        />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{ asset('/') }}css/style.min.css" />
        @toastr_css
        <style>
            #toast-container {
                left: 50%;
                transform: translateX(-50%);
                top: 60%;
                right: unset;
            }
        </style>
    </head>
    <body style="background-color: #e3e0e0;">
        <div class="container">
            <div class="row">
                <div
                    class="col-xs-12 col-sm-12 col-md-12 col-lg-5 p-5 mt-5"
                    style="margin: 0 auto;"
                >
                    <div
                        class="deg-block-login"
                        style="    padding: 50px 35px;
                        background-color: #efefef;
                        border-radius: 6px;
                        box-shadow: 0px 0px 7px 0px #000000;"
                    >
                        <form action="{{ route('postLogin') }}" method="POST">
                            @csrf()
                            <div class="form-group">
                                <input
                                    type="email"
                                    class="form-control"
                                    placeholder="Enter email"
                                    id="emailLogin"
                                    name="email"
                                    required
                                />
                            </div>
                            <div class="form-group">
                                <input
                                    type="password"
                                    class="form-control"
                                    placeholder="Enter password"
                                    id="pass"
                                    name="passWord"
                                    required
                                />
                            </div>
                            <div class="form-group form-check">
                                <label class="form-check-label">
                                    <input
                                        class="form-check-input"
                                        type="checkbox"
                                    />
                                    Remember me
                                </label>
                            </div>
                            <button
                                type="submit"
                                class="btn-primary"
                                style="    width: 100%;
                                border-radius: 3px;
                                padding: 5px;
                                outline: none;
                                border: 0px !important;"
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    @toastr_js @toastr_render
</html>
