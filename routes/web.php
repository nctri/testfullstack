<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'/'],function(){
    Route::get('/','client\homeCotroller@index')->name('index');
    Route::post('postSelectFace','client\homeCotroller@postSelectFace')->name('postSelectFace');
    Route::post('postCusRegister','client\homeCotroller@postCusRegister')->name('postCusRegister');
});

Route::group(['prefix'=>'merchant','middleware' => 'adminMiddleware'],function(){
    Route::get('/','admin\adminController@index')->name('adminIndex');
    Route::get('/postLogout','admin\loginController@postLogout')->name('postLogout');

    Route::get('/setting','admin\adminController@settingStore')->name('settingStore');
    Route::get('/postSettingStore','admin\adminController@postSettingStore')->name('postSettingStore');

});


Route::get('/login','admin\loginController@getLogin')->name('getLogin');
Route::post('/login','admin\loginController@postLogin')->name('postLogin');

