-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2020 at 07:11 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_cate` int(11) NOT NULL COMMENT '(1: lọc theo mặt tiền; 0: lọc theo thể loại)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `type_cate`, `created_at`, `updated_at`) VALUES
(1, 'Nhà Phố', 0, NULL, NULL),
(2, 'Nhà Quê', 0, NULL, NULL),
(8, 'Mặt tiền', 1, NULL, NULL),
(9, 'Trong hẽm', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_registed`
--

CREATE TABLE `email_registed` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_registed`
--

INSERT INTO `email_registed` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'nguyenthihuong150998@gmail.com', '2019-01-21 21:02:46', '2019-01-21 21:02:46'),
(2, 'nctri0110@gmail.com', '2019-01-21 21:05:28', '2019-01-21 21:05:28'),
(3, 'nguyenthihuo^^^ng15998@gmail.com', '2019-01-21 21:05:59', '2019-01-21 21:05:59'),
(4, 'dddddđ', '2019-01-21 21:43:00', '2019-01-21 21:43:00'),
(5, 'cobelangcam0605@yahoo.com', '2019-01-21 21:47:52', '2019-01-21 21:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Nhà Phố', '/', NULL, NULL),
(2, 'Biệt Thự', '#biet-thu', NULL, NULL),
(3, 'Dịch vụ', '#dich-vu', NULL, NULL),
(4, 'Vẽ tre nghệ', '#ve-tre-nghe', NULL, NULL),
(5, 'Liên hệ', '#lien-he', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cate_id` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_type` int(11) NOT NULL COMMENT '(1: Mặt tiền; 0: Trong Hẽm)',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `cate_id`, `product_image`, `name`, `short_description`, `long_description`, `home_type`, `created_at`, `updated_at`) VALUES
(1, 1, '1.jpg', 'Nhà Phố', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 1, NULL, NULL),
(2, 1, '2.jpg', 'Nhà Phố 1', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 1, NULL, NULL),
(3, 1, '3.jpg', 'Nhà Phố 2', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 1, NULL, NULL),
(4, 1, '4.jpg', 'Nhà Phố 3', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 1, NULL, NULL),
(5, 1, '5.jpg', 'Nhà Phố 4', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 1, NULL, NULL),
(6, 2, '6.jpg', 'Nhà Quê 5', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 2, NULL, NULL),
(7, 2, '7.jpg', 'Nhà Quê 6', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 2, NULL, NULL),
(8, 2, '8.jpg', 'Nhà Quê 7', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 2, NULL, NULL),
(9, 2, '9.jpg', 'Nhà Quê 8', 'Thiết Kế Cải Tạo Nhà Phố 3 Tầng Mặt Tiền 4m Tại Quận 11 – NP26', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `business_phone` varchar(12) DEFAULT NULL,
  `business_email` varchar(255) DEFAULT NULL,
  `business_social` varchar(255) DEFAULT NULL,
  `business_address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `logo`, `business_name`, `business_phone`, `business_email`, `business_social`, `business_address`) VALUES
(1, 'logo.png', 'TRENGHE', '0888 200 655', 'Trengheproduction@construct.com', 'https://www.instagram.com/accounts/login/;https://id.zalo.me/account?continue=https://chat.zalo.me;https://www.facebook.com/', 'Số 130 Thống Nhất - TP Hải Dương');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privilege` int(10) NOT NULL COMMENT '0:user 1:admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_registed`
--
ALTER TABLE `email_registed`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_registed_email_unique` (`email`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `email_registed`
--
ALTER TABLE `email_registed`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
